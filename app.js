const API = 'https://ajax.test-danit.com/api/json/';
const cardContainer = document.querySelector('.container')
const divCard = document.querySelector('.card__wrap')
console.log(divCard)

const sendRequest = async (entity, method = "GET", config) => {
    return await fetch(`${API}${entity}`, {
        method,
        ...config
    }).then(response => {
        if(response.ok){
            return response.json()
        } else {
            return new Error ('Что то пошло не так :(')
        }
    })
} 

const getUsers =  () =>  sendRequest('users');
const getPost =  () =>  sendRequest(`posts`)
const deletePost =  (id) =>  sendRequest(`posts/${id}`, "DELETE")
// const editPost =  (id) =>  sendRequest(`posts/${id}`)




Promise.all([getUsers(),getPost()]).then(data =>{
    const users = data[0];
    const posts = data[1];
    console.log(users);
    console.log(posts);
    users.forEach(({name,username,email}) => {
        posts.forEach(({title,body,id})=>{
     
     divCard.insertAdjacentHTML('afterbegin',`
     <div class='card'>
             <h2 class='title__field'>Full name:</h2>
             <p class='text__field'>${name}</p>
             <h2 class='title__field'>User name:</h2>
             <p class='text__field'>${username}</p>
             <h2 class='title__field'>Email:</h2>
             <p class='text__field'>${email}</p>
             <h2 class='title__field'>Title:</h2>
             <p class='text__field'>${title}</p>
             <h2 class='title__field'>Post:</h2>
             <p class='text__field'>${body}</p>
             <h2 class='title__field'>PostID:</h2>
             <p class='text__field'>${id}</p>
                 <div class="btn__wrapper"><button class="btn__delete">Delete post</button></div>
        <div> 
     
     `)
     const deleteBtn = document.querySelector('.btn__delete')
    deleteBtn.addEventListener('click',()=>{
        deletePost(id)
        const card = document.querySelector('.card');
        card.remove()
        alert('Deleted Post success')
    })
        })
   
    }); 
})

class Card {
    constructor(fullName,email,title,body,postId){
        this.fullName = fullName;
        this.email = email;
        this.title = title;
        this.body = body;
        this.postId = postId;
    }
}